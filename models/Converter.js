module.exports = {
  execute: params => {
    var miles = params.miles;
    var km = params.km;

    if ((miles == undefined || miles == "") && (km == undefined || km == "")) {
      return { miles: 0, km: 0 };
    }

    if (miles && miles != "") {
      km = miles * 1.60934;
    } else {
      miles = km / 1.60934;
    }

    return { miles: miles, km: km };
  }
};
