const app = require("../../models/Converter");

describe("converter", function() {
  it("miles to km", function() {
    const result = app.execute({ miles: 1 });

    expect(result.miles).toBe(1);
    expect(result.km).toBe(1.60934);
  });

  it("km to miles", function() {
    const result = app.execute({ km: 1 });

    expect(result.miles).toBe(0.6213727366498067);
    expect(result.km).toBe(1);
  });

  it("empty values", function() {
    const result = app.execute({});

    expect(result.miles).toBe(0);
    expect(result.km).toBe(0);
  });
});
