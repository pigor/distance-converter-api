const express = require("express");
const app = express();
const converter = require("./models/Converter");

app.get("/", (req, res) =>
  res.send(
    "API para conversão de distâncias entre Milhas e Km. Exemplo: /converter/?miles=1"
  )
);
app.get("/distance", (req, res) => {
  process.stdout.write(
    "\nRECEIVE => miles: " + req.query.miles + " / km: " + req.query.km
  );

  var response = converter.execute(req.query);

  process.stdout.write("\nRESPONSE => " + JSON.stringify(response));

  res.send(response);
});

app.listen(3002);
